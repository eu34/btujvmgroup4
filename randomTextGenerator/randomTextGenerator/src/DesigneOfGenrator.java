import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class DesigneOfGenrator implements Initializable {
    @FXML
    public ComboBox alphabet;
    @FXML
    public Spinner numberOfSentences;
    @FXML
    public TextArea result;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alphabet.setValue("Georgian");
        alphabet.getItems().add("Georgian");
        alphabet.getItems().add("English");
    }

    private String abcEn[] = {"vdskhdks sdlknvols nsdv ois", "aksfhas askjfchaks adfh"};
    private String abcGeo[] = {"ჯდსაჰ ასლდკვნ ას ლასდკჯვ ასდვნლ", "კჯჰასფუკჰსე იასდგ", "კაჯსჰფ ასკჯჰ ასნ კჯას ნლკასჯჰნ"};

    public void generate(){
        String abc = alphabet.getValue().toString();
        int numberOfS = (int) numberOfSentences.getValue();
        String randomText = "";
        if(abc.equals("Georgian")){
            Random random = new Random();
            for(int i=0; i<numberOfS; i++){
                randomText += abcGeo[random.nextInt(0, abcGeo.length)] + "\n";
            }
        }
          result.setText(randomText);
//        result.setText("Test: "+abc+" - "+numberOfS);
    }

    public void clear(){
        result.clear();
    }
}
