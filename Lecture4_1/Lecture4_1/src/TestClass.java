public class TestClass {
    static void testMethod(){
        System.out.println("Test Started");
        int m[] = {3, 4, 5, 9};
        int y = 0;
        try{
            System.out.println("Try block started");
            System.out.println(m[7]);
            System.out.println(5/y);
            System.out.println("Try block finished");
        }catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println(e.getMessage()+" Arr");
        }finally {
            System.out.println("This is a Finally Block!!!");
        }

        System.out.println("Test Finished");
    }
}
