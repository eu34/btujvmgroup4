public class FamilyBudget {
    private int money;

    public void setMoney(int money){
        this.money = money;
    }

    public void changeMoney(int money){
        this.money += money;
    }

    public int getMoney(){
        return this.money;
    }

}
