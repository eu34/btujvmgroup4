import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable {
    Socket clientSocket;
    ObjectOutputStream out;
    String message;

    @Override
    public void run() {
        try {
            while(true) {
                clientSocket = new Socket(InetAddress.getByName("localhost"), 8081);
                System.out.print("Client: ");
                Scanner scanner = new Scanner(System.in);
                message = scanner.nextLine();
                out = new ObjectOutputStream(clientSocket.getOutputStream());
                out.writeObject(message);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


//        for(int i=0; i<20; i++) {
//            System.out.println("Client!!! -> "+i);
//        }
    }
}
