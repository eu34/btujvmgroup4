import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread{
    Socket socket;
    ObjectInputStream in;

    public void run(){
        try {
            ServerSocket s = new ServerSocket(8081);

            while (true) {
                socket = s.accept();
                in = new ObjectInputStream(socket.getInputStream());
                System.out.println("S-> " + in.readObject());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

//        for(int i=0; i<20; i++) {
//            System.out.println("Server!!! -> "+i);
//        }
    }
}
