import java.io.*;

public class Files {
    void createFile1(){
        try {
            FileWriter writer = new FileWriter("file1.txt", true);
            writer.write(" Python ");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void createFile2(){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("C:/jvm/file.txt"));
            writer.write("Hello Java");
            writer.close();
        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void readFile1(){
        try {
            FileReader reader = new FileReader("file1.txt");
            System.out.println((char) reader.read());
            reader.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    void readFile2(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("file1.txt"));
            System.out.println(reader.readLine());
            reader.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
